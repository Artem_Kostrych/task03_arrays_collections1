package secondtask;

public class Door {
    private Find find;
    private boolean open;

    public Find getFind() {
        return find;
    }

    public void setFind(Find find) {
        this.find = find;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public Door() {
        int rand = (int) (Math.random() * 2);
        if (rand == 0) {
            find= new Monster();
        } else {
            find = new Artifact();
        }
        open = false;
    }

    @Override
    public String toString() {
        return "\nDoor{" +
                "find=" + find +
                '}';
    }
}
