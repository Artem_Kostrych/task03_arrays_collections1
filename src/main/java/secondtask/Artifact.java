package secondtask;

public class Artifact implements Find {
    private int strength;

    public Artifact() {
        strength = (int) (Math.random() * 70) + 10;
    }

    public int getStrength() {
        return strength;
    }


    @Override
    public String toString() {
        return "\tArtifact{" +
                "\tstrength=\t" + strength +
                '}';
    }
}
